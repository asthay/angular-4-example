import { Routes, RouterModule } from '@angular/router';
 
import { AuthGuard } from "./_guards/index";
import { LoginComponent } from "./controllers/login/login.component";
import { HomeComponent } from "./controllers/home/home.component";
import { RegisterComponent } from "./controllers/register/register.component";
import { ForgotPasswordComponent } from './controllers/forgotPassword/forgot-password/forgot-password.component';

 
const appRoutes: Routes = [
    {
        path: '',
        component: HomeComponent,
        // children: [
        //     {
        //         path: 'login',
        //         loadChildren: './controllers/login/login.module#LoginModule'
        //     },
        // ]
    },
    {
        path: 'login',
        loadChildren: './controllers/login/login.module#LoginModule'
    },
    {
        path: 'dashboard',
        loadChildren: './controllers/dashboard/dashboard.module#DashboardModule',
        canActivate: [AuthGuard]
    },
    {
        path : 'forgot-password',
        component : ForgotPasswordComponent
    },

    // { path: '', component: LoginComponent, canActivate: [AuthGuard] },
    // { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    
    
    // otherwise redirect to home
    { path: '**', redirectTo: ''}
];
 
export const routing = RouterModule.forRoot(appRoutes);
