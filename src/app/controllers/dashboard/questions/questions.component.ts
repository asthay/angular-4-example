import { Router } from "@angular/router";
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormArray, FormGroup , Validators} from '@angular/forms';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.css']
})
export class QuestionsComponent implements OnInit {

  invoiceForm: FormGroup;
  
    constructor(
      private _fb: FormBuilder,
      private router: Router
    ) {
      this.createForm();
    }
  
    createForm(){
      this.invoiceForm = this._fb.group({
        itemRows: this._fb.array([])
      });
      this.invoiceForm.setControl('itemRows', this._fb.array([]));
    }

    ngOnInit(){
      this.invoiceForm = this._fb.group({
        itemRows: this._fb.array([this.initItemRows()]) // here
      });
    }
    initItemRows() {
      return this._fb.group({
          // list all your form controls here, which belongs to your form array
          id: [Date.now()],
          question: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(540)]],
          answer: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(540)]]
      });
  }
  
    get itemRows(): FormArray {
      return this.invoiceForm.get('itemRows') as FormArray;
    }
    saveUserData() {
      console.log(this.invoiceForm.value.itemRows)
      localStorage.setItem('user_data', JSON.stringify(this.invoiceForm.value.itemRows))
      this.router.navigate(['/dashboard']);
    }
  
    addNewRow() {
     
      // control refers to your formarray
      const control = <FormArray>this.invoiceForm.controls['itemRows'];
      // add new formgroup
    console.log('control',this.invoiceForm.controls['itemRows']['controls'].length)
      if(this.invoiceForm.controls['itemRows']['controls'].length < 5){
      control.push(this.initItemRows());
      }
      else{
        alert("Cant add more than 5 fields!")
      }
  }
  
  deleteRow(index: number) {
      // control refers to your formarray
      const control = <FormArray>this.invoiceForm.controls['itemRows'];
      // remove the chosen row
      control.removeAt(index);
  }
} 