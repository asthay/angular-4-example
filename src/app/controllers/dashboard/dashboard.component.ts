import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormArray, FormGroup , Validators} from '@angular/forms';

import { AlertService } from "../../_services/alert.service";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"]
})
export class DashboardComponent implements OnInit {
  public list = [];
  ngOnInit(): void {
    this.list = JSON.parse(localStorage.getItem('user_data'))
    console.log('list of data ', this.list)
  }
} 