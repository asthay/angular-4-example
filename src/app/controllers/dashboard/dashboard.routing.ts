import { Routes, RouterModule } from '@angular/router';
import {ModuleWithProviders} from "@angular/core";
import { DashboardComponent } from "./dashboard.component";
import { QuestionsComponent } from "./questions/questions.component";

export const dashboardRoutes: Routes = [
    {
        path: '',
        component: DashboardComponent,
        data: {
            pageTitle: 'Dashboard'
        }
    },
    {
        path: 'add',
        component: QuestionsComponent,
        data: {
            pageTitle: 'Add'
        }
    }
];

export const dashboardRouting: ModuleWithProviders = RouterModule.forChild(dashboardRoutes);

