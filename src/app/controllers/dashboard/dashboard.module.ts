import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { dashboardRouting } from "./dashboard.routing";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AlertService } from "../../_services/alert.service";
import { QuestionsComponent } from './questions/questions.component';

@NgModule({
  imports: [
    CommonModule,
    dashboardRouting,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [DashboardComponent, QuestionsComponent],
  providers : [
    AlertService
  ]
})
export class DashboardModule { }
