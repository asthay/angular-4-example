import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
 
import { AlertService, UserService } from '../../_services/index';
import { question } from "../../_models/questions";
 
@Component({
  moduleId: module.id,
  templateUrl: "register.component.html"
})
export class RegisterComponent implements OnInit {

  constructor(
    private router: Router,
    private userService: UserService,
    private alertService: AlertService
  ) {}

  public model: any = {
    firstName : '',
    username : '',
    email : '',
    password : '',
    c_password: '',
    sq1 : '',
    sqa1 : '',
    sq2 : '',
    sqa2 : '',
    sq: ''  
  };
  public flag: boolean;
  public loading = false;
  public secQues = [];

  public ngOnInit (): void {
    this.setSecurityQuestions();
  }
 
  public register() {
    this.loading = true;
    this.userService.create(this.model).subscribe(
      data => {
        // set success message and pass true paramater to persist the message after redirecting to the login page
        this.alertService.success("Registration successful", true);
        this.router.navigate(["/login"]);
      },
      error => {
        this.alertService.error(error);
        this.loading = false;
      }
    );
  }
  public getConfirmPassword(value) {   
    console.log('value.......... ', value) 
    if(value === this.model.password) {
       this.flag = true
       console.log('true');
    } else {
      this.flag = false;
      console.log('false');
    }
  }

  public setSecurityQuestions(){
    localStorage.setItem('security_questions', JSON.stringify(question));
    this.getSecurityQuestions();
  }

  public getSecurityQuestions() {
    this.secQues =JSON.parse(localStorage.getItem('security_questions'));
  }
}