import { question } from './../../../_models/questions';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  constructor() { }

  public showQuestions = false;
  public questions = [];
  public emailId: any;
  public user = {
    q1Id : '',
    q2Id : '',
    email : '',
    question1: '',
    question2: ''
  };
  public userBase = [];
  public password = '';
  public showPassword = false;

  public ngOnInit() {
  }

  public emailEntered(value: any) {
    if(value.toString().length > 0){
      this.emailId = value;
      this.getSecurityQuestions();
    }
    else{
      this.showQuestions = false;
    }
  }

  public getSecurityQuestions(){
    this.userBase = JSON.parse(localStorage.getItem('users'))
    let security_questions = JSON.parse(localStorage.getItem('security_questions'))
    this.userBase.map(user => {
      console.log('mp 1',user)
      if(this.emailId == user.email){
        console.log('first if')
        let sq1Id = user.sq1;
        let sq2Id = user.sq2;
        security_questions.map(question => {
          if(sq1Id == question.id || sq2Id == question.id){
            console.log('second if')
          this.questions.push(question)
          this.showQuestions = true;
          }
        })
      }
    })
    console.log('this.questions',this.questions)
  }

  public forgotPassword(){
    this.user.q1Id = this.questions[0].id
    this.user.q2Id = this.questions[1].id
    console.log('this.user', this.user)
    this.checkAuthenticity(this.user)
  }

  public checkAuthenticity(userData){
    this.userBase.map(user =>{
      if(user.email == userData.email ){
        if((userData.q1Id == user.sq1 && userData.q2Id == user.sq2) && (userData.question1 == user.sqa1 && userData.question2 == user.sqa2)){
          this.password = user.password
          this.showPassword = true;
        }
      }
    })
  }

}
